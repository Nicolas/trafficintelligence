# Description

This software project provides a set of tools developed by [Nicolas Saunier](https://nicolas.saunier.confins.net) at Polytechnique Montreal and his collaborators for transportation data processing, in particular road traffic, motorized and non-motorized. The project consists in particular in tools for the most typical transportation data type, trajectories, i.e. temporal series of positions.

The code is licensed under the MIT open source license (http://www.opensource.org/licenses/mit-license).

Contact me at nicolas.saunier [at] polymtl.ca.

[https://trafficintelligence.confins.net](https://trafficintelligence.confins.net) redirects here. 

# Documentation

The [documentation](docs/README.md) is available in the [docs](./docs) sub-folder.

# Code Access
To get the code, please go to [https://sourceforge.net/projects/traffic-intelligence/](https://sourceforge.net/projects/traffic-intelligence/) to access the mercurial repository. The code can be cloned with the following command:

```
$ hg clone http://nicolassaunier@hg.code.sf.net/p/traffic-intelligence/code <destination directory>
```

