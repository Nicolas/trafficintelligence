# Output Data Formats

## Road user trajectories
* table positions `CREATE TABLE positions (trajectory_id INTEGER, frame_number INTEGER, x_coordinate REAL, y_coordinate REAL, PRIMARY KEY(trajectory_id, frame_number))`
    * trajectory_id int
	* frame_number int
	* x_coordinate float
	* y_coordinate float
* table velocities (same as positions, for speed vectors)
* table objects `CREATE TABLE objects (object_id INTEGER, road_user_type INTEGER DEFAULT 0, n_objects INTEGER DEFAULT 1, PRIMARY KEY(object_id))`
	* object_id int
	* road_user_type int (key in table moving.userTypeNames) `userTypeNames = ['unknown', 'car', 'pedestrian', 'motorcycle', 'bicycle', 'bus', 'truck', 'automated']`
	* n_objects int (if counting number of objects grouped together)
* table objects_features: n features to 1 object `CREATE TABLE objects_features (object_id INTEGER, trajectory_id INTEGER, PRIMARY KEY(object_id, trajectory_id))`

A higher level UML diagram was presented in the paper [Large-scale automated proactive road safety analysis using video data by Paul St-Aubin, Nicolas Saunier and  Luis Miranda-Moreno](http://www.sciencedirect.com/science/article/pii/S0968090X15001485).

## Behaviour analysis
* prototypes with links (assignments) to objects
* points of interests
* Road user interactions
* Scene metadata

# Other formats
## Ground truth annotations: 
A ground truth format was implemented as a series of bounding boxes (or other bounding geometric form): `CREATE TABLE bounding_boxes (object_id INTEGER, frame_number INTEGER, x_top_left REAL, y_top_left REAL, x_bottom_right REAL, y_bottom_right REAL, PRIMARY KEY(object_id, frame_number))`

