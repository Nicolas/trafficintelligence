# Video Processing Parameters
There are four main categories of parameters for the tools (program and scripts) provided in this project, provided on the command line and in configuration files:

* input/output files and camera calibration information
* video processing parameters
* user detection, tracking and classification parameters
* safety analysis parameters

The first category is the most general, as it is required by several tools:

* input video filename
* output data filename, e.g., trajectory data in a [SQLite database](output-data-formats.md)
* homography filename for 3x3 matrix, that can be combined with camera information if image is distorted (e.g., with significant fish-eye effect):
    * intrinsic camera filename
    * distortion coefficients
* first and last frame numbers to process (or first frame number and number of frames to process)

# Configuration Files
Two configuration files were initially developed for the FB tracker, `tracking.cfg` for the tracking configuration and `classifier.cfg` to [classify road users](road-user-classification.md). Both follow the [ini format](https://en.wikipedia.org/wiki/INI_file). The first, `tracking.cfg` was initially developed for the FB tracker which relies on the [Boost program options library](https://www.boost.org/doc/libs/release/libs/program_options/). That library does not support section and allows to process both command line arguments and a configuration file. 

Command line arguments always take precedence over their values in configuration files (if used). 

## tracking.cfg

The `tracking.cfg` file is well documented, as well as the FB tracker if asking for the help (`-h` or `--help`). An example is provided in the [code repository](https://sourceforge.net/p/traffic-intelligence/code/ci/default/tree/tracking.cfg). The first part corresponds to the 

## classifier.cfg

## Updates

When a configuration file is updated (when adding new paramters, or unfortunately changing names) as in revision [82c06ad](https://sourceforge.net/p/traffic-intelligence/code/ci/82c06ad62254), your (now) old configuration file must be updated to run with the new FB tracker. 

The easiest is to use a graphical text editor that can highlight the difference between the new configuration file (the one in the parent directory of the project is always up to date) and your existing configuration file. Here are existing tools: [Meld](http://meldmerge.org/) and [kdiff](http://kdiff3.sourceforge.net/) (*NIX), [Notepad++](http://notepad-plus-plus.org/) with the Compare plugin (Windows). 

Systematic update of a large number of configuration files is not available: if you know of a framework to do that easily, please tell me. ;-)
